// Import the Product model
const Product = require("../models/Product");
const Order = require("../models/Order");
const User = require("../models/User");
const cloudinary = require("../cloudinary");

// Function to add a new product to the database
const addProduct = async (reqBody, isAdmin) => {
    try{
        // Check if the user is an admin before allowing the action
        if (isAdmin){
            // Check if the product already exists in the database
            const productExists = await Product.exists({ name: reqBody.name });

            // Upload the image to cloudinary
            const uploadedIMG = await cloudinary.uploader.upload(reqBody.image, {
                upload_preset: "E-commerce"
            });

            // If not create a new product
            if (!productExists && uploadedIMG) {
                // Create a new Product object based on the request body
                let newProduct = new Product({
                    name: reqBody.name,
                    description: reqBody.description,
                    category: reqBody.category,
                    price: reqBody.price,
                    stock: reqBody.stock,
                    image: uploadedIMG
                });
                
                // Save the new product to the database
                const savedProduct = await newProduct.save();
                if(savedProduct){
                    return true;
                } else {
                    return false;
                };
            } else {
                // Return an error message
                return "Product already exists.";
            };
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            console.log("hey");
            return "Only admins can perform this action.";
        };
    } catch(err){
        return err;
    };
};

// Function to get all products from the database
const getAllProducts = async (isAdmin) => {
    try {
        // Check if the user is an admin before allowing the action
        if(isAdmin){
            // Find all products in the database
            const retrivedProducts = await Product.find({});
            return retrivedProducts;
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            return false;
        };
    } catch(err){
        return err;
    };
};


// Function to get all active products from the database
const getAllActive = async (reqBody) => {
  try {
    let query = { isActive: true };

    if (reqBody.search) {
      query["$or"] = [
        { name: { $regex: reqBody.search, $options: "i" } },
        { description: { $regex: reqBody.search, $options: "i" } },
        { category: { $eq: reqBody.search } }
      ];
    };

    const foundActives = await Product.find(query);

    if (foundActives.length) {
      return foundActives;
    } else {
      return false;
    };
  } catch (err) {
    return err;
  };
};

// Function to get a single product from the database based on its ID
const getProduct = async (productId) => {
    try {
        // Find product in the database basd on its ID
        const product = await Product.findById(productId);

        if (product) {
            return product;
        } else {
            // If the product does not exist, return an error message.
            return false;
        };
    } catch (err){
        return err;
    };
};

// Function to update a product in the database based on its ID
const updateProduct = async (reqBody, productId, isAdmin) => {
    try {
        // Check if the user is an admin before allowing the action
        
        if(isAdmin){

            // Upload the image to cloudinary
            const uploadedIMG = await cloudinary.uploader.upload(reqBody.image, {
                upload_preset: "E-commerce"
            });

            // Find the product in the database based on its ID and update its properties
            const updatedProduct = await Product.findByIdAndUpdate(productId, {
                "name": reqBody.name,
                "description": reqBody.description,
                "category": reqBody.category,
                "price": reqBody.price,
                "stock": reqBody.stock,
                "image": uploadedIMG
            }, {new: true});

            // If the update is successfull return the updated product
            if(updatedProduct){
                return true;
            } else {
                // Else return an error message
                return false;
            };
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            return "Only admins can perform this action.";
        };
    } catch (err){
        return err;
    };
};

// Function to archive a product in the database based on its ID
const archiveProduct = async (productId, isAdmin) => {
    try {
        // Check if the user is an admin before allowing the action
        if(isAdmin){
            // Find the product in the database based on its ID and toggle isActive
            const foundProduct = await Product.findById(productId);

            if(foundProduct){
                // If foundProduct.isActive is true, set it to false, and vice versa
                foundProduct.isActive = !foundProduct.isActive;

                // Save the updated product to the database
                await foundProduct.save();

                // Return the updated product
                return true; 
            } else {
                // If no product is found with the given ID, return false
                return false;
            };
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            return "Only admins can perform this action.";
        };
    } catch (err){
        return err;
    };
};

// Function to checkout a product and updating its stock quantity in the database based on its ID
const checkoutProduct = async (productId, reqBody, userData) => {
    try {
        // Check if the user is an admin before allowing the action
        if(!userData.isAdmin) {
            // Find the product by its ID
            const foundProduct = await Product.findById(productId);

            if (!foundProduct) {
                return "Product does not exist.";
            };

            if(foundProduct){
                // Determine the quantity to be purchased, defaulting to 1 if not specified
                const quantity = reqBody.quantity || 1;

                // Check if the product has enough stock for the purchase
                if (foundProduct.stock < quantity) {
                // Return an error message when the products dint have enough stock
                return `Insufficient stock. Only ${foundProduct.stock} item(s) available.`;
                } else {
                    // Create a new order with the purchased product
                    let newOrder = new Order({
                        userId: userData.id,
                        products: [
                            {
                                productId: foundProduct._id,
                                name: foundProduct.name,
                                image: foundProduct.image,
                                quantity: quantity || 1,
                                subtotal: foundProduct.price * quantity
                            }
                        ],
                        totalAmount: foundProduct.price * quantity
                    });
                    
                     // Save the new order to the database
                    const createdOrder = await newOrder.save();
    
                    // Update the product's stock quantity and isActive (status)
                    foundProduct.stock -= quantity;
                    await foundProduct.save();
    
                    if (foundProduct.stock === 0) {
                        foundProduct.isActive = false;
                        await foundProduct.save();
                    };
    
                    // Return the created order if successful
                    if(createdOrder){
                        return true;
                    } else {
                        return false;
                    };
                };
            } else {
                return "Product does not exist.";
            };
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            return "Admins cannot perform this action.";
        };
    } catch(err) {
        return err;
    };
};

// Function to add a product to user's cart
const addToCart = async (userData, req) => {
    try {
      // Check if the user is an admin before allowing the action
      if (!userData.isAdmin) {
        // Find the product by ID
        const foundProduct = await Product.findById(req.params.productId);
  
        // Check if the product exists
        if (!foundProduct) {
          return "Product does not exist.";
        };
  
        // Find the user by ID and retrieve their cart
        const foundUser = await User.findById(userData.id);
  
        // Check if the user exists
        if (!foundUser) {
          return "User not found.";
        };
  
        // Check if the product is already in the user's cart
        const existingCartItemIndex = foundUser.cart.findIndex(
          (item) => item.productId === req.params.productId
        );
  
        if (existingCartItemIndex !== -1) {
          // Update the quantity of the existing cart item
          foundUser.cart[existingCartItemIndex].quantity += req.body.quantity || 1;
          foundUser.cart[existingCartItemIndex].subtotal = foundProduct.price * foundUser.cart[existingCartItemIndex].quantity;
  
          // Update the user's total amount based on the updated cart item
          foundUser.totalAmount += foundProduct.price * (req.body.quantity || 1);
  
          // Save the updated user object to the database
          const updatedUser = await foundUser.save();
          if (updatedUser) {
            // Return the updated cart if successful
            return updatedUser;
          } else {
            // Return a message indicating that the product does not exist
            return false;
          };
        } else if (foundProduct.stock < (req.body.quantity || 1)) {
          // Return a message indicating that the product doesn't have enough stock
          return `This product doesn't have enough stock. Only ${foundProduct.stock} item(s) available.`;
        } else {
          // Create a new cart item with the product details and add it to the user's cart
          const newCartItem = {
            productId: req.params.productId,
            image: foundProduct.image,
            name: foundProduct.name,
            price: foundProduct.price,
            stock: foundProduct.stock,
            quantity: req.body.quantity || 1,
            subtotal: foundProduct.price * (req.body.quantity || 1),
          };
          foundUser.cart.push(newCartItem);
  
          // Update the user's total amount based on the new cart item
          foundUser.totalAmount += newCartItem.subtotal;
  
          // Save the updated user object to the database
          const updatedUser = await foundUser.save();
          if (updatedUser) {
            // Return the updated cart if successful
            return updatedUser;
          } else {
            // Return a message indicating that the product does not exist
            return false;
          };
        };
      } else {
        // Return a message indicating that only admins are authorized to perform this action
        return "Admins cannot perform this action.";
      };
    } catch (err) {
      return err;
    };
};
  

// Export all functions for use in other modules
module.exports = {addProduct, getAllProducts, getAllActive, getProduct, updateProduct, archiveProduct, checkoutProduct, addToCart};