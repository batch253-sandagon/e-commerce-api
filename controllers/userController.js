// Import necessary packages/dependencies and models
const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Function to register a new user
const registerUser = async (reqBody) => {
    try {
        // Check if the user already exists
        const foundUser = await User.find({email: reqBody.email});
        if (!foundUser.length > 0){
            // Create a new user with hashed password and save to database
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10)
            });
            const savedUser = await newUser.save();

             // Check if user was saved successfully
            if (savedUser) {
                return true;
            } else {
                return false;
            };
        } else {
            // Return error message if user already exists
            return false;
        };
    } catch(err) {
        // Returns the err if something went wrong
        return err;
    };
};

// Function to authenticate user login
const loginUser = async (reqBody)=>{
    try {
        // Find user by email
        const foundUser = await User.findOne({email: reqBody.email});
        if(foundUser){
            // Check if password is correct
            const isPasswordCorrect = await bcrypt.compare(reqBody.password, foundUser.password);

            // Return access token if password is correct
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(foundUser)};
            } else {
                 // Return error message if password is incorrect
                return false;
            }
        } else {
            // Return error message if user does not exist
            return false;
        };
    } catch(err){
        // Returns the err if something went wrong
        return err;
    };
};

// Function to retrieve user details by user ID
const retriveUserDetails = async (userData) => {
    try {
        // Find the user in the database with the given ID
        const foundUser = await User.findById(userData.id);
        
        if(foundUser){
            // If the user is found, remove the password field and return the user object
            foundUser.password = "";
            return foundUser;
        } else {
            // If the user is not found, return an error message
            return false;
        };
    } catch(err){
        return err;
    };
};

// Function to retrieve all orders for a user by user ID
const getOrders = async (userId) => {
    try {
        // Find all orders in the database with the given user ID
        const foundOrders = await Order.find({userId: userId});

        if(foundOrders.length > 0){
            // If orders are found, return the orders array
            return foundOrders;
        } else {
            // If no orders are found, return an error message
            return false;
        };
    } catch(err) {
        return err;
    };
};

// Function to retrieve all orders for all users (Admins only)
const getAllOrders = async (isAdmin) => {
    try {
        // Check if the user is an admin before allowing the action
        if(isAdmin){
            // If the user is an admin, find all orders in the database
            const retrievedOrders = await Order.find({});

            if (retrievedOrders.length > 0){
                // If orders are found, return the orders array
                return retrievedOrders;
            } else {
                // If no orders are found, return a message
                return false;
            };
            
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            return "Only admins can perform this action.";
        };
    } catch(err){
        return err;
    };
};

// Function to update a user's role to admin (Admins only)
const updateUserRole = async (req) => {
    try {
        // Check if the user is an admin before allowing the action
        const userData = auth.decode(req.headers.authorization);
        if(userData.isAdmin){
            // If the user is an admin, find the user based on its ID
            const foundUser = await User.findById(req.body.userId);
            if(!foundUser){
                return false;
            };

            if(foundUser.isAdmin){
                return false;
            };

            // Update user's role to admin and password to an empty string
            const updatedUser = await User.findByIdAndUpdate(req.body.userId,
                {
                    "isAdmin": true
                },
                {new: true}
            );

            // Return the updated user's details when the update is successfull
            if(updatedUser){
                updatedUser.password = "";
                return true;
            } else {
                return false;
            };
        } else {
            // Return a message indicating that only admins are authorized to perform this action
            return false;
        };
    } catch (err){
        return err;
    };
};

// Function to retrieve user's own cart
const getCart = async (req) => {
    try {
        // Check if the user is an admin before allowing the action
        const userData = auth.decode(req.headers.authorization);

        // Check if the user is the owner of the cart before allowing the action
        if (!userData.isAdmin) {
             // Find the user by their ID
            const foundUser = await User.findById(userData.id);

            if (foundUser) {
                // Check if the user has any items in their cart
                if(foundUser.cart) {
                    return foundUser.cart;
                } else {
                    return false;
                }
            } else {
                return false;
            };
        } else {
            // Return an error message if the user is not authorized to view the cart
            return false;
        };
    } catch(err) {
        return err;
    };
};

// Function to update user's own cart 
const updateCart = async (reqBody, userData) => {
    try {
      // Check if the user is the owner of the cart before allowing the action
      if (!userData.isAdmin) {

        // Find the product to update in the cart
        const foundProduct = await Product.findById(reqBody.productId);
        if (!foundProduct) {
            return "Product not found.";
        };
        
        // Find the user's cart to update
        const foundUser = await User.findById(userData.id);
        if (!foundUser) {
            return "User not found.";
        };
        // Check if cart is empty before attempting to update a product in it
        if (!foundUser.cart.length) {
            return "Your cart is currently empty.";
        };
  
          // Find the existing product in the user's cart
        const existingProduct = foundUser.cart.find(
          (item) => item.productId === reqBody.productId
        );

        // Check if requested quantity exceeds product stock
        if (reqBody.quantity > foundProduct.stock) {
            return "Requested quantity exceeds product stock.";
        };
  
        // Calculate the new and old subtotals for the product in the cart
        const newSubtotal = reqBody.quantity * foundProduct.price;
        const oldSubtotal = existingProduct.subtotal;
  
        // Find the user by their ID and udate the user's cart and total amount
        const updatedUser = await User.findByIdAndUpdate(
          userData.id,
          {
            $set: {
              "cart.$[elem].quantity": reqBody.quantity,
              "cart.$[elem].subtotal": newSubtotal,
            },
            $inc: { totalAmount: newSubtotal - oldSubtotal },
          },
          {
            new: true,
            arrayFilters: [{ "elem.productId": reqBody.productId }],
          }
        );
  
        // Return the updated cart
        if (updatedUser) {
          return updatedUser.cart;
        } else {
          return false;
        }
      } else {
        // Return an error message if the user is not authorized for this action
        return false;
      }
    } catch (err) {
        return err;
    }
};

// Function to remove a product from user's own cart 
const removeProductFromCart = async (reqBody, userData) => {
    try {
        // Check if the user is the owner of the cart before allowing the action
        if (!userData.isAdmin) {
            const foundUser = await User.findById(userData.id);
            if (!foundUser) {
              return "User not found.";
            };
      
            // Check if the user's cart is empty
            if (!foundUser.cart.length) {
              return foundUser.cart;
            };
      
            // Find the index of the product to remove in the cart array
            const productIndex = foundUser.cart.findIndex(
              (item) => item.productId === reqBody.productId
            );
      
            // Return a message when the product to be removed is not found
            if (productIndex === -1) {
              return foundUser.cart;
            };
      
            // Delete the product from the cart and update the user's total amount
            const productToDelete = foundUser.cart[productIndex];
            const newTotalAmount = foundUser.totalAmount - productToDelete.subtotal;
      
            const updatedUser = await User.findByIdAndUpdate(
                userData.id,
              {
                $pull: {
                  cart: productToDelete,
                },
                $set: {
                  totalAmount: newTotalAmount < 0 ? 0 : newTotalAmount,
                },
              },
              { new: true }
            );
      
            // If the cart is not empty, return its updated contents, otherwise inform the user it is empty
            if (updatedUser.cart) {
              return updatedUser.cart;
            } else {
              return false;
            }
        } else {
            // Return an error message if the user is not authorized for this action
          return false;
        };
    } catch (err) {
        return err;
    };
};

// Function to checkout all the products from user's own cart
const checkoutCart = async (reqBody, userData) => {
    try {
      // Check if user is not admin and is authorized to perform checkout
      if (!userData.isAdmin) {
        // Find the user by ID and check if it exists then get their cart
        const foundUser = await User.findById(userData.id);
        if (!foundUser) {
          return "User not found.";
        };
  
        const cartProducts = foundUser.cart;
  
        // Check if cart is empty
        if (cartProducts.length === 0) {
          return "Your cart is currently empty.";
        }
  
        // Filter the cart to only include checked products
        const checkedProducts = cartProducts.filter((cartProduct) => {
          return reqBody.checkedProductIds.some((checkedId) => checkedId === cartProduct.productId);
        });
  
        // Check if any checked product is out of stock
        let outOfStockProducts = [];
        for (const checkedProduct of checkedProducts) {
          const foundProduct = await Product.findById(checkedProduct.productId);
          if (foundProduct.stock < checkedProduct.quantity) {
            outOfStockProducts.push(foundProduct.name);
          }
        }
  
        // Return message with out-of-stock products if any
        if (outOfStockProducts.length > 0) {
          return `The following products don't have enough stock: ${outOfStockProducts.join(", ")}`;
        }
  
        // Decrease stock for each checked product in the cart
        for (const checkedProduct of checkedProducts) {
          const foundProduct = await Product.findById(checkedProduct.productId);
          foundProduct.stock -= checkedProduct.quantity;
          await foundProduct.save();
  
          // Set product to inactive if stock is now 0
          if(foundProduct.stock === 0){
            foundProduct.isActive = false;
            await foundProduct.save();
          };
        };
  
        // Calculate the total amount of the checked out products
        let checkedProductsTotalAmount = 0;
        for (const checkedProduct of checkedProducts) {
          checkedProductsTotalAmount += checkedProduct.quantity * checkedProduct.price;
        }
  
        // Create new order and save to database
        const newOrder = new Order({
          userId: userData.id,
          products: checkedProducts,
          totalAmount: checkedProductsTotalAmount,
        });
        const createdOrder = await newOrder.save();
  
        // Return the created order if successful
        if (createdOrder) {
          // Remove the checked products from the user's cart.
          foundUser.cart = cartProducts.filter((cartProduct) => {
            return !reqBody.checkedProductIds.includes(cartProduct.productId);
          });
  
          // Update the user's total amount by subtracting the checked products' total amount
          foundUser.totalAmount -= checkedProductsTotalAmount;
  
          await foundUser.save();
          return true;
        } else {
          return false;
        };
      } else {
        // Return an error message if the user is not authorized for this action
        return false;
      };
    } catch (err) {
      return err;
    };
};
  


// Export all functions for use in other modules
module.exports = {registerUser, loginUser, updateUserRole, retriveUserDetails, getAllOrders, getOrders, getCart, updateCart, removeProductFromCart, checkoutCart};