// Import Mongoose package to define the order schema and create the its model
const mongoose = require("mongoose");

// Define the order schema using Mongoose
const orderSchema = mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User ID is required"]
    },
    products: [
        {
          productId: {
            type: String,
            required: [true, "Product ID is required"]
          },
          image: {
            type: Object,
            required: [true, "Image is required"]
          },
          name: {
            type: String,
            required: [true, "Product name is required"]
          },
          quantity: {
            type: Number,
            default: 0
          },
          subtotal: {
            type: Number,
            default: 0
          }
        }
    ],
    totalAmount: {
        type: Number,
        default: 0
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    }
});

// Export the Order model using the order schema
module.exports = mongoose.model("Order", orderSchema)