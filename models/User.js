// Import Mongoose package
const mongoose = require("mongoose");

// Define the user schema using Mongoose
const userSchema = new mongoose.Schema({
    firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: [
		{
		 productId: {
			 type: String,
			 required: [true, "Product ID is required"]
		 },
		 image: {
			type: Object,
			required: [true, "Image is required"]
		 },
		 name: {
			 type: String,
			 required: [true, "Product ID is required"]
		 },
		 price: {
			type: Number,
			required: [true, "Price is required"]
		 },
		 stock: {
			type: Number,
			required: [true, "Stock is required"]
		 },
		 quantity: {
			 type: Number,
			 min: [1, 'Quantity can not be less then 1.'],
			 default: 0
		 },
		 subtotal: {
			 type: Number,
			 default: 0
		 }
		}
 	],
	totalAmount: {
		type: Number,
		default: 0
	}
}, { timestamps: true });

// Export the User model using the user schema
module.exports = mongoose.model("User", userSchema);