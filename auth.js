// Import required dependencies
const jwt = require("jsonwebtoken");
require("dotenv");

// Set up secret key for signing and verifying tokens
const secret = process.env.JWT_SECRET;

// Function for creating JWT access tokens
const createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };

    return jwt.sign(data, secret, {});
};

// Middleware for verifying JWT access tokens
const verify = (req, res, next) => {
    // Extract token from authorization header
    let token = req.headers.authorization;

    if(typeof token !== "undefined"){
 
        // Remove "Bearer " from token
        token = token.slice(7, token.length);

        // Verify token and call next middleware if successful
        return jwt.verify(token, secret, (err, data) => {
            if(!err){

                next();
            } else {
                // Return an error response if verification fails
                return res.send({auth: "failed"});
            }
        });
    } else {
        // Return an error response if no token is provided
        return res.send({auth: "failed"});
    };
};

// Function for decoding JWT access tokens
const decode = (token) => {
    if(typeof token !== "undefined"){

        // Remove "Bearer " from token
        token = token.slice(7, token.length);

        // Verify and decode token payload
        return jwt.verify(token, secret, (err, data) => {
            if(!err){
                return jwt.decode(token, {complete: true}).payload;
            } else {
                return null;
            };
        });
    } else {
        return null;
    };
};

// Export functions for creating, verifying, and decoding JWT access tokens
module.exports = {createAccessToken, verify, decode};