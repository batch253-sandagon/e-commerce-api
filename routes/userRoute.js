// Import Express package and create an Express router
const express = require("express");
const router = express.Router();

// Import userController and auth modules
const userController = require("../controllers/userController.js");
const auth = require("../auth");

// Route and controller for user registration
router.post("/register", async (req, res) => {
    try {
        // Call registerUser function from userController
        const result = await userController.registerUser(req.body);
        res.status(201).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for user login (Requires authentication)
router.post("/login", async (req, res) => {
    try {
        // Call loginUser function from userController
        const result = await userController.loginUser(req.body);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    };
});

// Route and controller for retrieving user details
router.get("/userDetails", async (req, res) => {
    try {
        const userData = auth.decode(req.headers.authorization);
        // Call retriveUserDetails function from userController
        const result = await userController.retriveUserDetails(userData);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
  });

// Route and controller for retrieving all orders (Requires authentication and Admin access)
router.get("/orders", auth.verify, async (req, res) => {
    try {
        // Decode user data from authorization header
        const userData = auth.decode(req.headers.authorization);

        // Call getOrders function from userController
        const result = await userController.getAllOrders(userData.isAdmin);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for retrieving user's own orders (Requires authentication)
router.get("/myOrders", auth.verify, async (req, res) => {
    // Decode user data from authorization header
    const userData = auth.decode(req.headers.authorization);
    try {
        // Call getOrders function from userController
        const orders = await userController.getOrders(userData.id);
        res.status(201).json(orders);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for updating user role to admin (Requires authentication and Admin access)
router.patch("/setAsAdmin", auth.verify, async (req, res)=>{
    try {
        // Call updateUserRole function from userController passing req object and userId as parameters
        const result = await userController.updateUserRole(req);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for retreiving user's cart (Requires authentication)
router.get("/myCart", auth.verify, async (req, res) => {
    try {
        // Call getCart function from userController with decoded user data
        const result = await userController.getCart(req, req.params.userId);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for updating user's cart (Requires authentication)
router.patch("/myCart/update", auth.verify, async (req, res) => {
    try {
        // Decode user data from authorization header
        const userData = auth.decode(req.headers.authorization);

        // Call getCart function from userController with decoded user data
        const result = await userController.updateCart(req.body, userData);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for deleting a product in user's cart (Requires authentication)
router.delete("/myCart/remove", auth.verify, async (req, res) => {
    try {
        // Decode user data from authorization header
        const userData = auth.decode(req.headers.authorization);

        // Call getCart function from userController with decoded user data
        const result = await userController.removeProductFromCart(req.body, userData);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Route and controller for  user's cart checkout(Requires authentication)
router.post("/myCart/checkout", auth.verify, async (req, res) => {
    try {
        const userData = auth.decode(req.headers.authorization);

        // Call getCart function from userController with decoded user data
        const result = await userController.checkoutCart(req.body, userData);
        res.status(201).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("An unexpected error occurred.");
    }
});

// Export the router
module.exports = router;